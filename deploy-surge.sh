# Build reactjs app with production mode
npm run Build

# Move to build folder
cd build
# Clone index.html into 200.html
cp index.html 200.html

# Start deploying via Surge
# The command means deploy current folder to domain cinemax-moives.surge.sh
surge . cinemax-movies.surge.sh