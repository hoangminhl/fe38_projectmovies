import React, { Component, Fragment } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { HomeTemplate } from "./templates/HomeTemplate/HomeTemplate";
import { AdminTemplate } from "./templates/AdminTemplate/AdminTemplate";

import "antd/dist/antd.css";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import Detail from "./pages/Detail/Detail";
import Checkout from "./pages/Checkout/Checkout";
import Profile from "./pages/Profile/Profile";
import Register from "./pages/Register/Register";
import MovieManagement from "./pages/MovieManagement/MovieManagement";
import MemberManagement from "./pages/MemberManagement/MemberManagement";

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Switch>
            <AdminTemplate
              exact
              path="/moviemanagement"
              Component={MovieManagement}
            />
            <AdminTemplate
              exact
              path="/membermanagement"
              Component={MemberManagement}
            />

            <HomeTemplate exact path="/" Component={Home} />
            <HomeTemplate exact path="/home" Component={Home} />
            <HomeTemplate
              exact
              path="/moviedetail/:maPhim"
              Component={Detail}
            />
            <HomeTemplate exact path="/login" Component={Login} />
            <HomeTemplate exact path="/register" Component={Register} />
            <HomeTemplate exact path="/userinformation" Component={Profile} />
            <Route exact path="/checkout/:maLichChieu" component={Checkout} />

          </Switch>
        </Fragment>
      </BrowserRouter>
    );
  }
}
