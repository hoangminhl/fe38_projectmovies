import axios from "axios";
import {domain, token, userLogin, groupID} from "../config/setting";

export class QuanLyNguoiDung {
    dangNhap = (userLogin) => {
        return axios ({
            url: `${domain}/QuanLyNguoiDung/DangNhap`,
            method: "POST",
            data: userLogin
        })
    }

    datVe = (thongTinDatVe) => {
        return axios ({
            url: `${domain}/QuanLyDatVe/DatVe`,
            method: "POST",
            data: thongTinDatVe,
            headers: {
                "Authorization": "Bearer " + localStorage.getItem(token)
            }
        })
    }

    layThongTinTaiKhoan = (taiKhoan) => {
        return axios ({
            url: `${domain}/QuanLyNguoiDung/ThongTinTaiKhoan`,
            method: "POST",
            data: taiKhoan,
        })
    }

    dangKy = (thongTin) => {
        return axios ({
            url: `${domain}/QuanLyNguoiDung/DangKy`,
            method: "POST",
            data: thongTin,
        })
    }

    layDanhSachNguoiDung = () => {
        return axios ({
            url: `${domain}/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${groupID}`,
            method: "GET"
        })
    }

    xoaNguoiDung = (taiKhoan) => {
        return axios ({
            url: `${domain}/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`,
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + localStorage.getItem(token)
            }
        })
    }

    capNhatNguoiDung = (nguoiDung) => {
        return axios ({
            url: `${domain}/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
            method: "PUT",
            data: nguoiDung,
            headers: {
                Authorization: "Bearer " + localStorage.getItem(token)
            }
        })
    }

    themNguoiDung = (addUser) => {
        return axios ({
            url: `${domain}/QuanLyNguoiDung/ThemNguoiDung`,
            method: "POST",
            data: addUser,
            headers: {
                Authorization: "Bearer " + localStorage.getItem(token)
            }
        })
    }
}

export const qlNguoiDung = new QuanLyNguoiDung();