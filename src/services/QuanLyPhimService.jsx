import axios from "axios";
import { domain, token, userLogin, groupID, groupAddID } from "../config/setting";

export class QuanLyPhimService {

    layDanhSachPhim = () => {
        return axios ({
            url: `${domain}/quanlyphim/laydanhsachphim?manhom=${groupID}`,
            method: 'GET'
        });
    }

    layDanhSachPhimSapChieu = () => {
        return axios ({
            url: `${domain}/quanlyphim/laydanhsachphim?manhom=${groupAddID}`,
            method: 'GET'
        });
    }

    layDanhSachPhimChieuRap = (maHeThongRap, maNhom) => {
        return axios ({
            url: `${domain}/QuanLyRap/LayThongTinLichChieuHeThongRap?maHeThongRap=${maHeThongRap}&maNhom=${maNhom}`,
            method: 'GET'
        });
    }

    layThongTinHeThongRap = () => {
        return axios ({
            url: `${domain}/QuanLyRap/LayThongTinHeThongRap`,
            method: 'GET'
        })
    }

    layThongTinCumRap = (maHeThongRap) => {
        return axios ({
            url: `${domain}/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${maHeThongRap}`,
            method: 'GET'
        })
    }

    layThongTinLichChieuPhim = (maPhim) => {
        return axios ({
            url: `${domain}/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`,
            method: 'GET'
        })
    }

    layThongTinPhongVe = (maLichChieu) => {
        return axios ({
            url: `${domain}/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`,
            method: 'GET'
        })
    }

    capNhatPhim = (phim) => {
        return axios ({
            url: `${domain}/QuanLyPhim/CapNhatPhim`,
            method: "POST",
            data: phim,
            headers: {
                Authorization: "Bearer " + localStorage.getItem(token),
            }
        })
    }

    themPhim = (form_data) => {
        return axios ({
            url: `${domain}/QuanLyPhim/ThemPhimUploadHinh`,
            method: "POST",
            data: form_data,
            headers: {
                Authorization: "Bearer " + localStorage.getItem(token),
            }
        })
    }

    xoaPhim = (maPhim) => {
        return axios ({
            url: `${domain}/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`,
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + localStorage.getItem(token),
            }
        })
    }
}

export const qlPhimService = new QuanLyPhimService();