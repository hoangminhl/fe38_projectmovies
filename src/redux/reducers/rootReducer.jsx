import {combineReducers} from 'redux';
import QuanLyNguoiDungReducer from "./QuanLyNguoiDungReducer";

//store tổng cho ứng dụng
export const rootReducer = combineReducers ({
    //Nơi chứa các reducer cho nghiệp vụ (store con)
   QuanLyNguoiDungReducer
})