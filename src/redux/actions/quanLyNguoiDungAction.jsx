import { DANG_NHAP, DANG_XUAT, DANG_KY } from "../types/quanLyNguoiDungType";

export const dangNhapAction = (taiKhoan) => {
    return {
        type:DANG_NHAP,
        taiKhoan,
    }
}

export const dangXuatAction = () => {
    return {
        type: DANG_XUAT,
    }
}
