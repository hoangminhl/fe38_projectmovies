import React, { Fragment } from 'react'
import RegisterComponent from '../../components/Register/RegisterComponent'

const Register = (props) => {
    let thongTin = props
    return (
        <Fragment>
            <RegisterComponent thongTin={thongTin}/>
        </Fragment>
    )
}

export default Register