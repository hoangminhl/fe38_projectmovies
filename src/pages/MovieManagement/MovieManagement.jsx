import React, { Fragment } from 'react'
import MoviesComponent from '../../components/Admin/MoviesComponent/MoviesComponent'
import Member from '../../components/Admin/Member/Member'

const MovieManagement = () => {
    return (
        <Fragment>
            <MoviesComponent/>
        </Fragment>
    )
}

export default MovieManagement