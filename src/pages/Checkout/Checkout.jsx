import React, { Fragment } from 'react'
import CheckoutHeader from '../../components/CheckoutHeader/CheckoutHeader'
import CheckoutLeft from '../../components/CheckoutLeft/CheckoutLeft'
import CheckoutSeat from '../../components/CheckoutSeat/CheckoutSeat'

const Checkout = (props) => {

    let maLichChieu = props.match.params.maLichChieu

    return (
        <Fragment>
            <CheckoutHeader maLichChieu={maLichChieu}/>
            <CheckoutLeft maLichChieu={maLichChieu}/>
            <CheckoutSeat maLichChieu={maLichChieu}/>
        </Fragment>
    )
}

export default Checkout