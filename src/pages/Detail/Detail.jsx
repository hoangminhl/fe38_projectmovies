import React, { Fragment } from 'react'
import DetailBackGround from '../../components/DetailBackGround/DetailBackGround'

const Detail = (props) => {
    var maPhim = props.match.params.maPhim;
    console.log(maPhim)
    return (
        <Fragment>
            <DetailBackGround maPhim = {maPhim}/>
        </Fragment>
    )
}

export default Detail