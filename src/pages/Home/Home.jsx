import React, { Fragment } from "react";
import Carousel from "../../components/Carousel/Carousel";
import Calendar from "../../components/Calendar/Calendar";
import HomeMovies from "../../components/HomeMovies/HomeMovies";
import News from "../../components/News/News";
import Application from "../../components/Application/Application";

const Home = () => {
  return (
    <Fragment>
      <div className="bg-dark">
        <Carousel />
        <Calendar />
        <HomeMovies />
        <News />
        <Application />
      </div>
    </Fragment>
  );
};

export default Home;
