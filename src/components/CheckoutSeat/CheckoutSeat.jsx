import React, { useState, useEffect, Fragment } from "react";
import "./CheckoutSeat.scss";
import { qlPhimService } from "../../services/QuanLyPhimService";
import CheckoutRight from "../CheckoutRight/CheckoutRight";
import swal from "sweetalert";

const CheckoutSeat = (props) => {
  let [thongTinPhongVe, setThongTinPhongVe] = useState([]);
  let [danhSachGheDangDat, setDanhSachGheDangDat] = useState([]);

  useEffect(() => {
    qlPhimService
      .layThongTinPhongVe(props.maLichChieu)
      .then((result) => {
        setThongTinPhongVe(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  const renderDanhSachGhe = () => {
    let { danhSachGhe } = thongTinPhongVe;

    return danhSachGhe?.map((ghe) => (
      <Fragment>
        {renderGhe(ghe.daDat, ghe)}
        {/* {(index + 1) % 16 === 0 ? <br /> : ""} */}
      </Fragment>
    ));
  };

  const datGhe = (ghe) => {
    let index = danhSachGheDangDat.findIndex(
      (gheDangDat) => gheDangDat.stt === ghe.stt
    );
    if (index !== -1) {
      danhSachGheDangDat.splice(index, 1);
    } else {
      danhSachGheDangDat = [...danhSachGheDangDat, ghe];
    }

    setDanhSachGheDangDat([...danhSachGheDangDat]);
  };

  const renderGhe = (daDat, ghe) => {
    if (daDat) {
      return (
        <div className="seatCheckOut__item">
          <button className={"ghe gheDaDat btn-block"} disabled>
            X
          </button>
        </div>
      );
    } else {
      let cssGheDangDat = "";
      let index = danhSachGheDangDat.findIndex(
        (gheDangDat) => gheDangDat.stt === ghe.stt
      );
      if (index !== -1) {
        cssGheDangDat = "gheDangDat";
      }

      let cssGheVip = "";
      if (ghe.loaiGhe === "Vip") {
        cssGheVip = "gheVip";
      }

      return (
        <div className="seatCheckOut__item">
          <button
            className={`ghe ${cssGheVip} ${cssGheDangDat} btn-block`}
            onClick={() => {
              datGhe(ghe);
            }}
          >
            {ghe.stt}
          </button>
        </div>
      );
    }
  };

  const [counter, setCounter] = useState(60);
  useEffect(() => {
    counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
    if (counter === 0) {
      swal("Bạn đã chọn vé quá lâu!", {
        icon: "error",
      });
      setTimeout(() => {
        window.location.reload();
      }, 3000);
    }
  }, [counter]);

  return (
    <Fragment>
      <CheckoutRight maLichChieu={props.maLichChieu} danhSachGheDangDat={danhSachGheDangDat}/>

      <div className="seatCheckOut">
        <div className="seatCheckOut__content">
          <div className="seatCheckOut__header">
            <div className="seatCheckOut__leftTitle">
              <div className="seatCheckOut__logo">
                <img
                  // src="../img/ee621ee05dcd4565caead4f29421b41e.png"
                  src={thongTinPhongVe.thongTinPhim?.hinhAnh}
                  alt="logo"
                />
              </div>
              <div className="seatCheckOut__contentCinema">
                <p className="seatCheckOut__address">
                  {thongTinPhongVe.thongTinPhim?.tenCumRap}
                </p>
                <p className="seatCheckOut__hour">
                  {" "}
                  - {thongTinPhongVe.thongTinPhim?.gioChieu} -{" "}
                  {thongTinPhongVe.thongTinPhim?.tenRap}
                </p>
              </div>
            </div>
            <div className="seatCheckOut__rightTitle">
              <p className="seatCheckOut__info1">Thời gian giữ ghế</p>
              <p className="seatCheckOut__info2">{counter + "s"}</p>
            </div>
          </div>
          <div className="clear" />
          <div className="seatCheckOut__seatMap">
            <div className="seatCheckOut__screen">
              <img src="../img/screen.png" alt="icon" />
            </div>
            <div className="seatCheckOut__listSeat">{renderDanhSachGhe()}</div>
            <div className="seatCheckOut__noteSeat">
              <div className="seatCheckOut__typeSeats">
                <span className="seatCheckOut__colorChoosing seatCheckOut__typeSeat">
                  <span className="color" />
                  <span className="nameSeat">Ghế đang chọn</span>
                </span>
                <span className="seatCheckOut__colorChosen seatCheckOut__typeSeat">
                  <span className="color" />
                  <span className="nameSeat">Ghế đã có người chọn</span>
                </span>
                <span className="seatCheckOut__colorVIP seatCheckOut__typeSeat">
                  <span className="color" />
                  <span className="nameSeat">Ghế VIP</span>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default CheckoutSeat;
