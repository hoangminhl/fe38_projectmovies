import React, { useState, useEffect } from "react";
import "./HomeMovies.scss";
import { qlPhimService } from "../../services/QuanLyPhimService";
import { NavLink } from "react-router-dom";

var moment = require("moment");

const HomeMovies = () => {
  let mangImg = [
    { tenRap: "CGV", img: "./img/ImgCinema/cgv-cinema.jpg" },
    { tenRap: "BHDStar", img: "./img/ImgCinema/bhd-cinema.jpg" },
    { tenRap: "Galaxy", img: "./img/ImgCinema/galaxy-cinema.jpg" },
    { tenRap: "LotteCinima", img: "./img/ImgCinema/lotte-cinema.jpg" },
    { tenRap: "MegaGS", img: "./img/ImgCinema/mega-cinema.jpg" },
    { tenRap: "CineStar", img: "./img/ImgCinema/ddc-cinema.jpg" },
  ];

  let [img, setImg] = useState([]);
  let [rap, setRap] = useState([]);
  let [cumRap, setCumRap] = useState([]);
  let [danhSachPhim, setDanhSachPhim] = useState([]);

  useEffect(() => {
    qlPhimService.layThongTinHeThongRap().then((result) => {
      setRap(result.data);
    });
  }, []);

  useEffect(() => {
    setImg(mangImg);
  }, []);

  // useEffect(() => {
  //   qlPhimService.layDanhSachPhimChieuRap().then((result) => {
  //     console.log(result.data);
  //     setDanhSachPhim(result.data);
  //   });
  // }, []);

  const layCumRap = (maHeThongRap) => {
    qlPhimService
      .layThongTinCumRap(maHeThongRap)
      .then((result) => {
        setCumRap(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });

    qlPhimService
      .layDanhSachPhimChieuRap(maHeThongRap, "GP09")
      .then((result) => {
        setDanhSachPhim(result.data);
      });
  };

  const renderRap = () => {
    return rap?.map((item, index) => {
      return (
        <a
          className="nav-link"
          id="v-pills-cinema-tab"
          data-toggle="pill"
          href={`#${item.maHeThongRap}`}
          role="tab"
          aria-controls="v-pills-CGV"
          aria-selected="true"
          key={index}
          onClick={() => {
            layCumRap(item.maHeThongRap);
          }}
        >
          <img src={item.logo} alt={item.logo} />
        </a>
      );
    });
  };

  const renderCumRap = () => {
    return rap?.map((item, index) => {
      return (
        <div
          key={index}
          className="tab-pane fade"
          id={item.maHeThongRap}
          role="tabpanel"
          aria-labelledby="v-pills-cinema-tab"
        >
          <div className="homeMovies__scope">
            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
              {cumRap?.map((itemCR, index) => {
                return (
                  <li className="nav-item w-100" key={index}>
                    <a
                      className="homeMovies__cinema nav-link"
                      id="v-pills-home-tab"
                      data-toggle="pill"
                      href={`#${itemCR.maCumRap}`}
                      role="tab"
                      aria-control="pill-aeonBinhTan"
                      aria-selected="true"
                    >
                      <div className="homeMovies__picture">
                        {img.map((imgItem, index) => {
                          if (imgItem.tenRap === item.maHeThongRap) {
                            return (
                              <img src={imgItem.img} alt="icon" key={index} />
                            );
                          }
                        })}
                      </div>
                      <div className="homeMovies__text">
                        <p className="homeMovies__nameMovieCinema">
                          {itemCR.tenCumRap}
                        </p>
                        <p className="homeMovies__infoMovieCinema">
                          {itemCR.diaChi}
                        </p>
                        <p className="homeMovies__showingDetailCinema">
                          <a href="#">[chi tiết]</a>
                        </p>
                      </div>
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      );
    });
  };

  const renderDanhSachPhim = () => {
    return danhSachPhim?.map((CumRap) => {
      return CumRap.lstCumRap?.map((listCumRap, index) => {
        return (
          <div
            key={index}
            className="tab-pane fade"
            id={listCumRap.maCumRap}
            role="tabpanel"
            aria-labelledby="v-pills-home-tab"
          >
            <div className="homeMovies__contentMovies">
              {listCumRap.danhSachPhim?.map((phim, index) => {
                return (
                  <div key={index}>
                    <p>
                      <a
                        data-toggle="collapse"
                        href={`#id${phim.maPhim}`}
                        role="button"
                        aria-expanded="false"
                        aria-controls="AeonBinhTan"
                      ></a>
                    </p>
                    <div className="homeMovies__movies active">
                      <a
                        data-toggle="collapse"
                        href={`#id${phim.maPhim}`}
                        role="button"
                        aria-expanded="false"
                        aria-controls={`id${phim.maPhim}`}
                      >
                        <div className="homeMovies__picture">
                          <img src={phim.hinhAnh} alt="icon" />
                        </div>
                        <div className="homeMovies__text">
                          <a
                            data-toggle="collapse"
                            href="#AeonBinhTan"
                            role="button"
                            aria-expanded="false"
                            aria-controls={`id${phim.maPhim}`}
                          ></a>
                          <p className="homeMovies__titleMovies">
                            <span className="homeMovies__2D">2D</span>
                            <span className="homeMovies__nameMovies">
                              {phim.tenPhim}
                            </span>
                          </p>
                          <p className="homeMovies__infoMovies">
                            90 phút - 7 - IMDb 6.9
                          </p>
                        </div>
                      </a>

                      <div className="collapse" id={`id${phim.maPhim}`}>
                        <div className="homeMovies__time">
                          {phim.lstLichChieuTheoPhim.map(
                            (lichChieuPhim, index) => {
                              return (
                                <NavLink
                                  type="button"
                                  to={`/checkout/${lichChieuPhim.maLichChieu}`}
                                  className="time"
                                  key={index}
                                >
                                  <span className="homeMovies__timeBegin">
                                    {moment(
                                      lichChieuPhim.ngayChieuGioChieu
                                    ).format("hh:mm A")}
                                  </span>
                                </NavLink>
                              );
                            }
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        );
      });
    });
  };

  return (
    <section className="homeMovies" id="cinemaBlock">
      <div className="homeMovies__content">
        <div className="homeMovies__bg">
          <div className="row">
            <div className="col-12">
              <div className="parentListPCinemas col-sm-12">
                <div
                  className="nav flex-column nav-pills"
                  id="v-pills-tab"
                  role="tablist"
                  aria-orientation="vertical"
                >
                  {renderRap()}
                </div>
              </div>
              <div className="listCinemas col-sm-12">
                <div
                  className="tab-content selectScroll"
                  id="v-pills-tabContent"
                >
                  {renderCumRap()}
                </div>
              </div>

              <div className="listMovies col-sm-12">
                <div className="tab-content selectScroll" id="pills-tabcontent">
                  {renderDanhSachPhim()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HomeMovies;
