import React, { useState } from "react";
import "./AddMovies.scss";
import { qlPhimService } from "../../../services/QuanLyPhimService";
import swal from "sweetalert";

var moment = require("moment");

const AddMovies = () => {
  let [state, setState] = useState({
    values: {
      maPhim: "",
      tenPhim: "",
      biDanh: "",
      hinhAnh: "",
      trailer: "",
      moTa: "",
      ngayKhoiChieu: "",
      danhGia: "",
      maNhom: "",
    },
    errors: {
      maPhim: "",
      tenPhim: "",
      biDanh: "",
      hinhAnh: "",
      trailer: "",
      moTa: "",
      ngayKhoiChieu: "",
      danhGia: "",
      maNhom: "",
    },
  });

  const handleChangeInput = (event) => {
    let { value, name } = event.target;
    let newValue = {
      ...state.values,
      [name]: value,
    };

    let newErrors = {
      ...state.errors,
      [name]: value === "" ? "Không được bỏ trống!" : "",
    };

    console.log(event.target.files)

    if (name === "hinhAnh") {
      newValue[name] = event.target.files[0];
    }

    if (name === "ngayKhoiChieu") {
      newValue[name] = moment(newValue[name]).format("DD-MM-yyyy");
    }

    setState({ values: newValue, errors: newErrors });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let valid = true;
    let { values, errors } = state;

    console.log(values)
    for (let key in values) {
      if (values[key] === "") {
        valid = false;
      }
    }

    for (let key in errors) {
      if (errors[key] !== "") {
        valid = false;
      }
    }

    if (!valid) {
      swal({
        title: "Thông tin không hợp lệ!",
        icon: "error",
        button: "OK",
      });
      return;
    }

    var form_data = new FormData();
    for (let key in state.values) {
        console.log(key, state.values[key])
      form_data.append(key, state.values[key])
    }

    console.log(state.values)
    qlPhimService
      .themPhim(form_data)
      .then((result) => {
        swal({
          title: "Thêm phim thành công!",
          icon: "success",
          button: "OK",
        });
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((errors) => {
        swal({
          title: errors.response.data,
          icon: "warning",
          button: "OK",
        });
      });
  };
  return (
    <div
      className="addMovies modal fade"
      id="addMovieModal"
      tabIndex={-1}
      aria-labelledby="addMovieModal"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="addMovieModal">
              Thêm phim
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">
            <form onSubmit={handleSubmit} className="form">
              <div className="row">
                <div className="col-sm-6">
                  <div className="textBox form-group">
                    <label className="text">Mã phim</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="maPhim"
                        onChange={handleChangeInput}
                      required
                    />
                    <span className="text-danger">{state.errors.maPhim}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Tên phim</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="tenPhim"
                      onChange={handleChangeInput}
                      required
                    />
                    <span className="text-danger">{state.errors.tenPhim}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Bí danh</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="biDanh"
                      onChange={handleChangeInput}
                    />
                    <span className="text-danger">{state.errors.biDanh}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Hình ảnh</label>
                    <input
                      className="content form-control"
                      type="file"
                      name="hinhAnh"
                      onChange={handleChangeInput}
                    />
                    <span className="text-danger">{state.errors.hinhAnh}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Mã nhóm</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="maNhom"
                      onChange={handleChangeInput}
                    />
                    <span className="text-danger">{state.errors.maNhom}</span>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="textBox form-group">
                    <label className="text">Trailer</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="trailer"
                      onChange={handleChangeInput}
                    />
                    <span className="text-danger">{state.errors.trailer}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Mô tả</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="moTa"
                      onChange={handleChangeInput}
                      required
                    />
                    <span className="text-danger">{state.errors.moTa}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Ngày khởi chiếu</label>
                    <input
                      className="content form-control"
                      type="date"
                      name="ngayKhoiChieu"
                      min="today"
                      onChange={handleChangeInput}
                      required
                    />
                    <span className="text-danger">
                    </span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Đánh giá</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="danhGia"
                      onChange={handleChangeInput}
                      required
                    />
                    <span className="text-danger">{state.errors.danhGia}</span>
                  </div>
                </div>
              </div>
              <button className="btn btn-primary">OK</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddMovies;
