import React, { useState, Fragment } from "react";
import swal from "sweetalert";
import { qlNguoiDung } from "../../../services/QuanLyNguoiDungService";
import { groupID } from "../../../config/setting";

const AddMember = () => {
  let [state, setState] = useState({
    values: {
      taiKhoan: "",
      matKhau: "",
      hoTen: "",
      email: "",
      soDt: "",
      maLoaiNguoiDung: "",
      maNhom: groupID,
    },
    errors: {
      taiKhoan: "",
      matKhau: "",
      hoTen: "",
      email: "",
      soDt: "",
      maLoaiNguoiDung: "",
      maNhom: "",
    },
  });

  const handleChangeInput = (event) => {
    let { value, name } = event.target;
    let newValue = {
      ...state.values,
      [name]: value,
    };

    let newErrors = {
      ...state.errors,
      [name]: value === "" ? "Không được bỏ trống!" : "",
    };

    if (name === "email") {
      let regexEmail = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
      if (value.match(regexEmail)) {
        newErrors.email = "";
      } else {
        newErrors.email = "Email không hợp lệ!";
      }
    }

    console.log(event);

    // if (name === "hinhAnh") {
    //   newValue[name] = event.target.files[0];
    // }

    setState({ values: newValue, errors: newErrors });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let valid = true;
    let { values, errors } = state;

    console.log(values);
    for (let key in values) {
      if (values[key] === "") {
        valid = false;
      }
    }

    for (let key in errors) {
      if (errors[key] !== "") {
        valid = false;
      }
    }

    if (!valid) {
      swal({
        title: "Thông tin không hợp lệ!",
        icon: "error",
        button: "OK",
      });
      return;
    }

    // var form_data = new FormData();
    // for (let key in state.values) {
    //     console.log(key, state.values[key]
    //   form_data.append(key, state.values[key]);
    // }

    console.log(state.values);
    qlNguoiDung
      .themNguoiDung(values)
      .then((result) => {
        swal({
          title: "Thêm phim thành công!",
          icon: "success",
          button: "OK",
        });
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((errors) => {
        swal({
          title: errors.response.data,
          icon: "warning",
          button: "OK",
        });
      });
  };
  return (
    <Fragment>
      <div
        className="addMovies modal fade"
        id="addMemberModal"
        tabIndex={-1}
        aria-labelledby="addMemberModal"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="addMemberModal">
                Thêm phim
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <form onSubmit={handleSubmit} className="form">
                <div className="row">
                  <div className="col-sm-6">
                    <div className="textBox form-group">
                      <label className="text">Tài khoản</label>
                      <input
                        className="content form-control"
                        type="text"
                        name="taiKhoan"
                        onChange={handleChangeInput}
                        //   value={state.values.maPhim}
                        //   style={{ cursor: "no-drop" }}
                        required
                      />
                      <span className="text-danger">
                        {state.errors.taiKhoan}
                      </span>
                    </div>
                    <div className="textBox form-group">
                      <label className="text">Mật khẩu</label>
                      <input
                        className="content form-control"
                        type="password"
                        name="matKhau"
                        onChange={handleChangeInput}
                        //   value={state.values.tenPhim}
                        required
                      />
                      <span className="text-danger">
                        {state.errors.matKhau}
                      </span>
                    </div>
                    <div className="textBox form-group">
                      <label className="text">Họ tên</label>
                      <input
                        className="content form-control"
                        type="text"
                        name="hoTen"
                        onChange={handleChangeInput}
                        //   value={state.values.biDanh}
                      />
                      <span className="text-danger">{state.errors.hoTen}</span>
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="textBox form-group">
                      <label className="text">Email</label>
                      <input
                        className="content form-control"
                        type="text"
                        name="email"
                        onChange={handleChangeInput}
                        //   value={state.values.trailer}
                      />
                      <span className="text-danger">{state.errors.email}</span>
                    </div>
                    <div className="textBox form-group">
                      <label className="text">Số điện thoại</label>
                      <input
                        className="content form-control"
                        type="text"
                        name="soDt"
                        onChange={handleChangeInput}
                        //   value={state.values.moTa}
                        required
                      />
                      <span className="text-danger">{state.errors.soDt}</span>
                    </div>
                    <div className="textBox form-group">
                      <label className="text">Người dùng</label>
                      <select
                        className="content form-control"
                        name="maLoaiNguoiDung"
                        onChange={handleChangeInput}
                        required
                      >
                        <option value="#">--Chọn Loại Người Dùng--</option>
                        <option value="KhachHang">Khách hàng</option>
                        <option value="QuanTri">Quản trị</option>
                      </select>
                      <span className="text-danger">
                        {/* {state.errors.ngayKhoiChieu} */}
                      </span>
                    </div>
                  </div>
                </div>
                <button className="btn btn-primary">OK</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default AddMember;
