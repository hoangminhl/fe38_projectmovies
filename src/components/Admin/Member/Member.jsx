import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import "./Member.scss";
import swal from "sweetalert";
import { qlNguoiDung } from "../../../services/QuanLyNguoiDungService";
import EditMember from "../EditMember/EditMember";
import AddMember from "../AddMember/AddMember";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 680,
  },
});

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#343a40",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const Member = () => {
  let [danhSachNguoiDung, setDanhSachNguoiDung] = useState([]);

  useEffect(() => {
    qlNguoiDung
      .layDanhSachNguoiDung()
      .then((result) => {
        setDanhSachNguoiDung(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  const renderDanhSachPhim = () => {
    return danhSachNguoiDung
      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
      ?.map((user, index) => {
        return (
          <TableRow hover role="checkbox" tabIndex={-1} key={index}>
            <TableCell>{index + 1}</TableCell>
            <TableCell>{user.taiKhoan}</TableCell>
            <TableCell>{user.hoTen}</TableCell>
            <TableCell>{user.email}</TableCell>
            <TableCell>{user.soDt}</TableCell>
            <TableCell>{user.maLoaiNguoiDung}</TableCell>
            <TableCell>
              <div
                className="edit-action mr-2"
                style={{ cursor: "pointer", display: "inline" }}
              >
                <i
                  className="fas fa-edit"
                  data-toggle="modal"
                  data-target={`#id${user.taiKhoan}`}
                ></i>
              </div>
              <EditMember user={user} />
              <div className="delete-action" style={{ display: "inline" }}>
                <i
                  className="fas fa-trash-alt"
                  style={{ cursor: "pointer", color: "red" }}
                  onClick={() => {
                    swal({
                      title: `Bạn có chắc chứ?`,
                      text: `Xóa phim ${user.hoTen}`,
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                    })
                      .then((willDelete) => {
                        if (willDelete) {
                          qlNguoiDung.xoaNguoiDung(user.taiKhoan).then((result) => {
                            swal({
                              title: "Xóa thành công!",
                              icon: "success",
                              button: "OK",
                            });
                            qlNguoiDung
                              .layDanhSachNguoiDung()
                              .then((results) => {
                                setDanhSachNguoiDung(results.data);
                              })
                              .catch((errors) => {
                                console.log(errors.response.data);
                              });
                          });
                        }
                      })
                      .catch((err) => {
                        swal({
                          title: "Xóa không thành công!",
                          icon: "warning",
                          button: "OK",
                        });
                      });
                  }}
                ></i>
              </div>
            </TableCell>
          </TableRow>
        );
      });
  };

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <div className="user">
        <h3>Danh Sách Người Dùng</h3>
      </div>
      <button
        className="button"
        data-toggle="modal"
        data-target="#addMemberModal"
      >
        <i class="fas fa-plus" style={{ color: "white" }}></i>
      </button>
      <AddMember/>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <StyledTableCell>STT</StyledTableCell>
              <StyledTableCell>Tài khoản</StyledTableCell>
              <StyledTableCell>Họ tên</StyledTableCell>
              <StyledTableCell>Email</StyledTableCell>
              <StyledTableCell>Số ĐT</StyledTableCell>
              <StyledTableCell>Người dùng</StyledTableCell>
              <StyledTableCell>Thao tác</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>{renderDanhSachPhim()}</TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={danhSachNguoiDung.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default Member;
