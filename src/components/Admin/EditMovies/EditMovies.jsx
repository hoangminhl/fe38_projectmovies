import React, { useState } from "react";
import "./EditMovies.scss";
import swal from "sweetalert";
import { qlPhimService } from "../../../services/QuanLyPhimService";
import { groupID } from "../../../config/setting";

var moment = require("moment");

const EditMovies = (props) => {
  let phim = props.phim;

  let [state, setState] = useState({
    values: {
      maPhim: phim.maPhim,
      tenPhim: phim.tenPhim,
      biDanh: phim.biDanh,
      hinhAnh: phim.hinhAnh,
      trailer: phim.trailer,
      moTa: phim.moTa,
      ngayKhoiChieu: phim.ngayKhoiChieu,
      danhGia: phim.danhGia,
      maNhom: groupID,
    },
    errors: {
      maPhim: "",
      tenPhim: "",
      biDanh: "",
      hinhAnh: "",
      trailer: "",
      moTa: "",
      ngayKhoiChieu: "",
      danhGia: "",
    },
  });

  const handleChangeInput = (event) => {
    let { value, name } = event.target;
    let newValue = {
      ...state.values,
      [name]: value,
    };

    let newErrors = {
      ...state.errors,
      [name]: value === "" ? "Không được bỏ trống!" : "",
    };

    console.log(newValue[name]);
    if (name === "ngayKhoiChieu") {
      newValue[name] = moment(newValue[name]).format("DD-MM-yyyy");
    }

    setState({ values: newValue, errors: newErrors });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let valid = true;
    let { values, errors } = state;

    for (let key in values) {
      if (values[key] === "") {
        valid = false;
      }
    }

    for (let key in errors) {
      if (errors[key] !== "") {
        valid = false;
      }
    }

    if (!valid) {
      swal({
        title: "Thông tin không hợp lệ!",
        icon: "error",
        button: "OK",
      });
      return;
    }

    qlPhimService
      .capNhatPhim(values)
      .then((result) => {
        swal({
          title: "Sửa phim thành công!",
          icon: "success",
          button: "OK",
        });
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((errors) => {
        swal({
          title: errors.response.data,
          icon: "waning",
          button: "OK",
        });
      });
  };

  return (
    <div
      className="editMovies modal fade"
      id={`id${phim.maPhim}`}
      tabIndex={-1}
      aria-labelledby="editModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="editModalLabel">
              Sửa phim
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">
            <form onSubmit={handleSubmit} className="form">
              <div className="row">
                <div className="col-sm-6">
                  <div className="textBox form-group">
                    <label className="text">Mã phim</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="maPhim"
                    //   onChange={handleChangeInput}
                      disabled
                      value={state.values.maPhim}
                      style={{ cursor: "no-drop" }}
                      required
                    />
                    <span className="text-danger">{state.errors.maPhim}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Tên phim</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="tenPhim"
                      onChange={handleChangeInput}
                      value={state.values.tenPhim}
                      required
                    />
                    <span className="text-danger">{state.errors.tenPhim}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Bí danh</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="biDanh"
                      onChange={handleChangeInput}
                      value={state.values.biDanh}
                    />
                    <span className="text-danger">{state.errors.biDanh}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Hình ảnh</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="hinhAnh"
                      onChange={handleChangeInput}
                      value={state.values.hinhAnh}
                    />
                    <span className="text-danger">{state.errors.hinhAnh}</span>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="textBox form-group">
                    <label className="text">Trailer</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="trailer"
                      onChange={handleChangeInput}
                      value={state.values.trailer}
                    />
                    <span className="text-danger">{state.errors.trailer}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Mô tả</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="moTa"
                      onChange={handleChangeInput}
                      value={state.values.moTa}
                      required
                    />
                    <span className="text-danger">{state.errors.moTa}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Ngày khởi chiếu</label>
                    <input
                      className="content form-control"
                      type="date"
                      name="ngayKhoiChieu"
                      onChange={handleChangeInput}
                      required
                    />
                    <span className="text-danger">
                      {state.errors.ngayKhoiChieu}
                    </span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Đánh giá</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="danhGia"
                      onChange={handleChangeInput}
                      value={state.values.danhGia}
                      required
                    />
                    <span className="text-danger">{state.errors.danhGia}</span>
                  </div>
                </div>
              </div>
              <button className="btn btn-primary">OK</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditMovies;
