import React, { useState } from "react";
import "./EditMember.scss";
import { groupID } from "../../../config/setting";
import swal from "sweetalert";
import { qlNguoiDung } from "../../../services/QuanLyNguoiDungService";

const EditMember = (props) => {
  let user = props.user;

  let [state, setState] = useState({
    values: {
      taiKhoan: user.taiKhoan,
      matKhau: user.matKhau,
      hoTen: user.hoTen,
      email: user.email,
      soDt: user.soDt,
      maNhom: groupID,
      maLoaiNguoiDung: user.maLoaiNguoiDung,
    },
    errors: {
      taiKhoan: "",
      matKhau: "",
      hoTen: "",
      email: "",
      soDt: "",
    },
  });

  const handleChangeInput = (event) => {
    let { value, name } = event.target;
    let newValue = {
      ...state.values,
      [name]: value,
    };

    let newErrors = {
      ...state.errors,
      [name]: value === "" ? "Không được bỏ trống!" : "",
    };

    setState({ values: newValue, errors: newErrors });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let valid = true;
    let { values, errors } = state;

    for (let key in values) {
      if (values[key] === "") {
        valid = false;
      }
    }

    for (let key in errors) {
      if (errors[key] !== "") {
        valid = false;
      }
    }

    if (!valid) {
      swal({
        title: "Thông tin không hợp lệ!",
        icon: "error",
        button: "OK",
      });
      return;
    }

    qlNguoiDung
      .capNhatNguoiDung(values)
      .then((result) => {
        swal({
          title: "Sửa phim thành công!",
          icon: "success",
          button: "OK",
        });
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((errors) => {
        swal({
          title: errors.response.data,
          icon: "waning",
          button: "OK",
        });
      });
  };

  return (
    <div
      className="editMember modal fade"
      id={`id${user.taiKhoan}`}
      tabIndex={-1}
      aria-labelledby="editModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="editModalLabel">
              Sửa phim
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">
            <form onSubmit={handleSubmit} className="form">
              <div className="row">
                <div className="col-sm-6">
                  <div className="textBox form-group">
                    <label className="text">Tài khoản</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="taiKhoan"
                      //   onChange={handleChangeInput}
                      disabled
                      value={state.values.taiKhoan}
                      style={{ cursor: "no-drop" }}
                      required
                    />
                    <span className="text-danger">{state.errors.taiKhoan}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Mật khẩu</label>
                    <input
                      className="content form-control"
                      type="password"
                      name="matKhau"
                      onChange={handleChangeInput}
                      value={state.values.matKhau}
                      required
                    />
                    <span className="text-danger">{state.errors.matKhau}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Họ tên</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="hoTen"
                      onChange={handleChangeInput}
                      value={state.values.hoTen}
                    />
                    <span className="text-danger">{state.errors.hoTen}</span>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="textBox form-group">
                    <label className="text">Email</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="email"
                      onChange={handleChangeInput}
                      value={state.values.email}
                    />
                    <span className="text-danger">{state.errors.email}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Số điện thoại</label>
                    <input
                      className="content form-control"
                      type="text"
                      name="soDt"
                      onChange={handleChangeInput}
                      value={state.values.soDt}
                      required
                    />
                    <span className="text-danger">{state.errors.soDt}</span>
                  </div>
                  <div className="textBox form-group">
                    <label className="text">Ngày khởi chiếu</label>
                    <select
                      className="content form-control"
                      name="maLoaiNguoiDung"
                      onChange={handleChangeInput}
                      value={state.values.maLoaiNguoiDung}
                      required
                    >
                        <option value="#">--Chọn loại người dùng--</option>
                        <option value="KhachHang">Khách hàng</option>
                        <option value="QuanTri">Quản trị</option>
                    </select>
                    <span className="text-danger">
                      {state.errors.maLoaiNguoiDung}
                    </span>
                  </div>
                  
                </div>
              </div>
              <button className="btn btn-primary">OK</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditMember;
