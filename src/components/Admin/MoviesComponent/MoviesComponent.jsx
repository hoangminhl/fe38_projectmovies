import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { qlPhimService } from "../../../services/QuanLyPhimService";
import "./MoviesComponent.scss";
import EditMovies from "../EditMovies/EditMovies";
import AddMovies from "../AddMovies/AddMovies";
import swal from "sweetalert";

var moment = require("moment");

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 680,
  },
});

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#343a40",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const MoviesComponent = () => {
  let [danhSachPhim, setDanhSachPhim] = useState([]);

  useEffect(() => {
    qlPhimService
      .layDanhSachPhim()
      .then((result) => {
        setDanhSachPhim(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  const renderDanhSachPhim = () => {
    return danhSachPhim
      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
      ?.map((phim, index) => {
        return (
          <TableRow hover role="checkbox" tabIndex={-1} key={phim.maPhim}>
            <TableCell>{phim.maPhim}</TableCell>
            <TableCell style={{ width: "100px" }}>{phim.tenPhim}</TableCell>
            <TableCell style={{ width: "100px" }}>{phim.biDanh}</TableCell>
            <TableCell>
              <img
                src={phim.hinhAnh}
                style={{ width: "60px", height: "90px" }}
              />
            </TableCell>
            <TableCell style={{ width: "200px" }}>{phim.trailer}</TableCell>
            <TableCell style={{ width: "400px" }}>
              <div className="text__description">{phim.moTa}</div>
            </TableCell>
            <TableCell>
              {moment(phim.ngayKhoiChieu).format("DD-MM-yyyy")}
            </TableCell>
            <TableCell>{phim.danhGia}</TableCell>
            <TableCell>
              <div
                className="edit-action mr-2"
                style={{ cursor: "pointer", display: "inline" }}
              >
                <i
                  className="fas fa-edit"
                  data-toggle="modal"
                  data-target={`#id${phim.maPhim}`}
                ></i>
              </div>
              <EditMovies phim={phim} />
              <div className="delete-action" style={{ display: "inline" }}>
                <i
                  className="fas fa-trash-alt"
                  style={{ cursor: "pointer", color: "red" }}
                  onClick={() => {
                    swal({
                      title: `Bạn có chắc chứ?`,
                      text: `Xóa phim ${phim.tenPhim}`,
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                    })
                      .then((willDelete) => {
                        if (willDelete) {
                          qlPhimService.xoaPhim(phim.maPhim).then((result) => {
                            swal({
                              title: "Xóa thành công!",
                              icon: "success",
                              button: "OK",
                            });
                            qlPhimService
                              .layDanhSachPhim()
                              .then((results) => {
                                setDanhSachPhim(results.data);
                              })
                              .catch((errors) => {
                                console.log(errors.response.data);
                              });
                          });
                        }
                      })
                      .catch((err) => {
                        swal({
                          title: "Xóa không thành công!",
                          icon: "warning",
                          button: "OK",
                        });
                      });
                  }}
                ></i>
              </div>
            </TableCell>
          </TableRow>
        );
      });
  };

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <button
        className="button"
        data-toggle="modal"
        data-target="#addMovieModal"
      >
        <i class="fas fa-plus" style={{ color: "white" }}></i>
      </button>
      <AddMovies />
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Mã phim</StyledTableCell>
              <StyledTableCell>Tên phim</StyledTableCell>
              <StyledTableCell>Bí danh</StyledTableCell>
              <StyledTableCell>Hình ảnh</StyledTableCell>
              <StyledTableCell>Trailer</StyledTableCell>
              <StyledTableCell>Mô tả</StyledTableCell>
              <StyledTableCell>Ngày khởi chiếu</StyledTableCell>
              <StyledTableCell>Đánh giá</StyledTableCell>
              <StyledTableCell>Thao tác</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>{renderDanhSachPhim()}</TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={danhSachPhim.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default MoviesComponent;
