import React, { useState, useEffect } from "react";
import "./DetailName.scss";
import { qlPhimService } from "../../services/QuanLyPhimService";
import TrailerMovie from "../DialogMovie/TrailerMovie";

var moment = require("moment");

const DetailName = (props) => {

  let maPhim = props.maPhim

  let [phim, setPhim] = useState([]);

  useEffect(() => {
    qlPhimService.layThongTinLichChieuPhim(maPhim).then((result) => {
      setPhim(result.data);
    });
  }, []);

  const renderTime = () => {
    return phim.heThongRapChieu?.slice(0,1).map((heThongRapChieu) => {
      return heThongRapChieu.cumRapChieu?.slice(0,1).map((cumRap) => {
        return cumRap.lichChieuPhim?.slice(0,1).map((lichChieu) => {
          return ( 
            <p className="name__time">{lichChieu.thoiLuong} phút - 0 IMDb - 2D/Digital</p>
          )
        })
      })
    })
  }

  return (
    <section className="name">
      <div className="name__content">
        <div className="name__background">
          <div className="name__picture">
            <img src={phim.hinhAnh} alt="background" />
          </div>
          <div className="name__overlay" />
          <div className="name__items">
            <div className="name__cover">
              <div className="row">
                <div className="col-md-3 col-sm-12">
                  <div className="name__logo">
                    <img
                      src={phim.hinhAnh}
                      alt={phim.biDanh}
                      className="picture"
                    />
                    <button className="name__playTrailer">
                      <TrailerMovie trailer={phim.trailer}/>
                    </button>
                  </div>
                </div>
                <div className="col-5">
                  <div className="name__text">
                    <p className="name__date">
                      {moment(phim.ngayKhoiChieu).format("DD-MM-yyyy")}
                    </p>
                    <p className="name__film">
                      <span>{phim.maNhom}</span> {phim.tenPhim}
                    </p>
                    {renderTime()}
                    <div className="name__booking">
                      <button className="booking__button">Mua vé</button>
                    </div>
                  </div>
                </div>
                <div className="col-2">
                  <div className="name__circlePercent c100 p65">
                    <div className="name__circleBorder" />
                    <span className="name__number">{phim.danhGia}</span>
                    <div className="slice">
                      <div className="bar haftCircle" />
                      <div className="fill haftCircle" />
                    </div>
                  </div>
                  <div className="row name__star">
                    <img src="../img/star1.png" alt="star1" />
                    <img src="../img/star1.png" alt="star1" />
                    <img src="../img/star1.png" alt="star1" />
                    <img src="../img/star1.2.png" alt="star1.2" />
                  </div>
                  <div className="row name__rating">
                    <p>65 người đánh giá</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default DetailName;
