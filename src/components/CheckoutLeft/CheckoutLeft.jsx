import React, { useState, useEffect } from "react";
import "./CheckoutLeft.scss";
import { qlPhimService } from "../../services/QuanLyPhimService";

const CheckoutLeft = (props) => {
  let [thongTinPhongVe, setThongTinPhongVe] = useState([]);

  useEffect(() => {
    qlPhimService
      .layThongTinPhongVe(props.maLichChieu)
      .then((result) => {
        setThongTinPhongVe(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  return (
    <div className="left">
      <div className="left__content">
        <div
          className="left__bg"
          style={{
            backgroundImage: `url(${thongTinPhongVe.thongTinPhim?.hinhAnh})`,
          }}
        >
          <div className="left__overlay" />
        </div>
      </div>
    </div>
  );
};

export default CheckoutLeft;
