import React, { useState, useEffect, Fragment } from "react";
import "./Carousel.scss";
import { qlPhimService } from "../../services/QuanLyPhimService";
import TrailerCarousel from "../Dialog/TrailerCarousel";
import { NavLink } from "react-router-dom";

var moment = require("moment");

const Carousel = () => {
  let [danhSachPhim, setDanhSachPhim] = useState([]);
  let [thongTinPhim, setThongTinPhim] = useState([]);
  let [maPhim, setMaPhim] = useState({});
  let [maCumRap, setMaCumRap] = useState();
  let [ngayChieu, setNgayChieu] = useState();
  let [maLichChieu, setMaLichChieu] = useState();

  var handleInput = (event) => {
    let maPhim = parseInt(event.target.value); // parseInt: chuyển đổi từ chuỗi thành số
    setMaPhim(maPhim);
  };

  var handleCinema = (event) => {
    let maCumRap = event.target.value;
    setMaCumRap(maCumRap);
  };

  var handleDate = (event) => {
    let ngayChieu = event.target.value;
    setNgayChieu(ngayChieu);
  };

  var handleInputLichChieu = (event) => {
    let maLichChieu = event.target.value;
    setMaLichChieu(maLichChieu);
  };

  useEffect(() => {
    qlPhimService
      .layDanhSachPhim()
      .then((result) => {
        setDanhSachPhim(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  useEffect(() => {
    qlPhimService
      .layThongTinLichChieuPhim(maPhim)
      .then((result) => {
        setThongTinPhim(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, [maPhim]);

  const renderDSPhim = () => {
    return danhSachPhim.map((phim, index) => {
      return (
        <option value={phim.maPhim} key={index}>
          {phim.tenPhim}
        </option>
      );
    });
  };

  const renderCumRap = () => {
    return thongTinPhim.heThongRapChieu?.map((cumRap) => {
      return cumRap.cumRapChieu?.map((rap, index) => {
        return (
          <option value={rap.maCumRap} key={index}>
            {rap.tenCumRap}
          </option>
        );
      });
    });
  };

  const groupBy = (array, key) => {
    return array.reduce((result, currentValue) => {
      (result[moment(currentValue[key]).format("DD-MM-yyyy")] =
        result[moment(currentValue[key]).format("DD-MM-yyyy")] || []).push(
        currentValue
      );

      return result;
    }, {});
  };

  const renderNgayChieu = () => {
    return thongTinPhim.heThongRapChieu?.map((cumRap) => {
      return cumRap.cumRapChieu?.map((rap) => {
        if (rap.maCumRap === maCumRap) {
          const listLichChieu = groupBy(rap.lichChieuPhim, "ngayChieuGioChieu");
          return Object.entries(listLichChieu).map(([value], i) => {
            return (
              <option value={value} key={i}>
                {value}
              </option>
            );
          });
        } else {
          return null;
        }
      });
    });
  };

  const renderSuatChieu = () => {
    return thongTinPhim.heThongRapChieu?.map((cumRap) => {
      return cumRap.cumRapChieu?.map((rap) => {
        if (maCumRap === rap.maCumRap) {
          // const listLichChieu = groupBy(rap.lichChieuPhim, "ngayChieuGioChieu");
          // console.log(listLichChieu);

          // return Object.entries(listLichChieu).map(([index, value], i) => {
          //   console.log(index, value);
          //   return value.map((item) => {
          //     if (maLichChieu === index) {
          //       return (
          //         <option
          //           value={index}
          //           // value={moment(lichChieu.ngayChieuGioChieu).format("DD:MM:yyyy")}
          //           key={i}
          //         >
          //           {moment(item.ngayChieuGioChieu).format("hh:mm A")}
          //         </option>
          //       );
          //     } else {
          //       return null;
          //     }
          //   });
          // });

          return rap.lichChieuPhim?.map((lichChieu, index) => {
            if (
              ngayChieu ===
              moment(lichChieu.ngayChieuGioChieu).format("DD-MM-yyyy")
            ) {
              return (
                <option key={index} value={lichChieu.maLichChieu}>
                  {moment(lichChieu.ngayChieuGioChieu).format("hh:mm A")}
                </option>
              );
            }
          });
        }
      });
    });
  };

  const renderDatVe = () => {
    if (maLichChieu) {
      return (
        <NavLink
          to={`/checkout/${maLichChieu}`}
          className="smallBlock widthByPercent "
        >
          <button className="btn btn-primary buyTicket" >MUA VÉ NGAY</button>
        </NavLink>
      );
    } else {
      return (
        <div
          className="smallBlock widthByPercent "
        >
          <button className="btn btn-primary buyTicket" disabled style={{cursor: "no-drop"}}>MUA VÉ NGAY</button>
        </div>
      )
    }
  };

  return (
    <section className="carousel">
      <div className="carousel__content">
        <div
          id="TixIndicators"
          className="carousel slide tixCarousel"
          data-ride="carousel"
        >
          <ol className="carousel-indicators">
            <li
              data-target="#TixIndicators"
              data-slide-to={0}
              className="active"
            />
            <li data-target="#TixIndicators" data-slide-to={1} />
            <li data-target="#TixIndicators" data-slide-to={2} />
            <li data-target="#TixIndicators" data-slide-to={3} />
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="carousel__changeDetail">
                <img
                  src="https://s3img.vcdn.vn/123phim/2020/07/ban-dao-15954792351353.jpg"
                  className="d-block w-100"
                  alt="ban-dao"
                />
                <div className="carousel__bg" />
              </div>
              <button type="button" className="carousel__button">
                <TrailerCarousel
                  trailer={
                    "https://www.youtube.com/embed/dgUAoEIeigo?autoplay=1"
                  }
                />
              </button>
            </div>
            <div className="carousel-item">
              <img
                src="https://s3img.vcdn.vn/123phim/2020/08/dinh-thu-oan-khuat-15967340117741.png"
                className="d-block w-100"
                alt="dinh-thu-oan-khuat"
              />
              <div className="carousel__bg" />
              <button type="button" className="carousel__button">
                <TrailerCarousel
                  trailer={
                    "https://www.youtube.com/embed/dsOSmQl2yA8?autoplay=1"
                  }
                />
              </button>
            </div>
            <div className="carousel-item">
              <img
                src="https://s3img.vcdn.vn/123phim/2020/08/bi-mat-thien-duong-15972163589211.jpg"
                className="d-block w-100"
                alt="bi-mat-thien-duong"
              />
              <div className="carousel__bg" />
              <button type="button" className="carousel__button">
                <TrailerCarousel
                  trailer={
                    "https://www.youtube.com/embed/5IMIdd3iq6A?autoplay=1"
                  }
                />
              </button>
            </div>
            <div className="carousel-item">
              <img
                src="https://s3img.vcdn.vn/123phim/2020/07/du-lich-chet-choc-15961360123636.jpg"
                className="d-block w-100"
                alt="du-lich-chet-choc"
              />
              <div className="carousel__bg" />
              <button type="button" className="carousel__button">
                <TrailerCarousel
                  trailer={
                    "https://www.youtube.com/embed/m8y4zigvplE?autoplay=1"
                  }
                />
              </button>
            </div>
          </div>
          <a
            className="carousel-control-prev"
            href="#TixIndicators"
            role="button"
            data-slide="prev"
          >
            <span
              className="carousel-control-prev-icon button-control"
              aria-hidden="true"
            />
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next"
            href="#TixIndicators"
            role="button"
            data-slide="next"
          >
            <span
              className="carousel-control-next-icon button-control"
              aria-hidden="true"
            />
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div className="carousel__selectMovie">
        <div className="w30p widthByPercent dropdown selectFilm">
          <select
            className="selectMenu"
            defaultValue={"DEFAULT"}
            name="movie"
            onChange={handleInput}
          >
            <option value="DEFAULT">Chọn phim</option>
            {renderDSPhim()}
          </select>
        </div>
        <div className="smallBlock widthByPercent dropdown selectCinema">
          <select
            className="selectMenu"
            defaultValue={"DEFAULT"}
            name="slct"
            onChange={handleCinema}
          >
            <option value="DEFAULT">Chọn rạp</option>
            {renderCumRap()}
          </select>
        </div>
        <div className="smallBlock widthByPercent dropdown selectDate">
          <select
            className="dropdown-toggle selectMenu"
            defaultValue={"DEFAULT"}
            name="slct"
            onChange={handleDate}
          >
            <option value="DEFAULT">Ngày chiếu</option>
            {renderNgayChieu()}
          </select>
        </div>
        <div className="smallBlock widthByPercent dropdown selectSession">
          <select
            className="dropdown-toggle selectMenu"
            defaultValue={"DEFAULT"}
            onChange={handleInputLichChieu}
          >
            <option value="DEFAULT">Suất chiếu</option>
            {renderSuatChieu()}
          </select>
        </div>
        {/* <div className="smallBlock widthByPercent ">
          <button className="btn btn-primary buyTicket">MUA VÉ NGAY</button>
        </div> */}
        {renderDatVe()}
      </div>
      <div className="clear" />
    </section>
  );
};

export default Carousel;
