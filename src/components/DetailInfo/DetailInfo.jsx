import React, { useState, useEffect } from "react";
import "./DetailInfo.scss";
import { qlPhimService } from "../../services/QuanLyPhimService";
import { NavLink } from "react-router-dom";

var moment = require("moment");

const DetailInfo = (props) => {
  let [phim, setPhim] = useState([]);
  useEffect(() => {
    qlPhimService
      .layThongTinLichChieuPhim(props.maPhim)
      .then((result) => {
        setPhim(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  const renderTime = () => {
    return phim.heThongRapChieu?.slice(0, 1).map((heThongRapChieu) => {
      return heThongRapChieu.cumRapChieu?.slice(0, 1).map((cumRap) => {
        return cumRap.lichChieuPhim?.slice(0, 1).map((lichChieu) => {
          return (
            <p className="info__time">
              {lichChieu.thoiLuong} phút - 0 IMDb - 2D/Digital
            </p>
          );
        });
      });
    });
  };

  const renderLogoCinema = () => {
    return phim.heThongRapChieu?.map((heThongRapChieu) => {
      return (
        <a
          className="nav-link"
          id="v-pills-cinema-tab"
          data-toggle="pill"
          href={`#${heThongRapChieu.maHeThongRap}`}
          role="tab"
          aria-controls="v-pills-cinema"
          aria-selected="true"
        >
          <img src={heThongRapChieu.logo} alt={heThongRapChieu.maHeThongRap} />
        </a>
      );
    });
  };

  const renderDate = () => {
    return phim.heThongRapChieu?.map((heThongRapChieu, index) => {
      return (
        <div
          class="tab-pane fade"
          id={heThongRapChieu.maHeThongRap}
          role="tabpanel"
          aria-labelledby="pills-cinema-tab"
          key={index}
        >
          <div class="info__items">
            {heThongRapChieu.cumRapChieu?.map((rap, index) => {
              return (
                <div>
                  <p>
                    <a
                      data-toggle="collapse"
                      href={`#${rap.maCumRap}`}
                      role="button"
                      aria-expanded="false"
                      aria-controls={rap.maCumRap}
                    ></a>
                  </p>
                  <div className="info__cinema active">
                    <a
                      data-toggle="collapse"
                      href={`#${rap.maCumRap}`}
                      role="button"
                      aria-expanded="false"
                      aria-controls={rap.maCumRap}
                    >
                      <div className="info__picture">
                        <img src={heThongRapChieu.logo} alt="CGV" />
                      </div>
                    </a>
                    <div className="info__text">
                      <a
                        data-toggle="collapse"
                        href={`#${rap.maCumRap}`}
                        role="button"
                        aria-expanded="false"
                        aria-controls={rap.maCumRap}
                      >
                        <p className="info__nameMovieCinema">
                          <span
                            className={`info__colorCinema-${heThongRapChieu.maHeThongRap}`}
                          >
                            {heThongRapChieu.maHeThongRap}
                          </span>{" "}
                          - {rap.tenCumRap}
                        </p>
                        <p className="info__infoMovieCinema">
                          Tầng 3, Trung tâm thương mại Aeon Mall Bình Tân, Số 1
                          đường số 17A, khu phố 11, phường Bình Trị Đông B, quận
                          Bình Tân, TPHCM
                        </p>
                      </a>
                      <p className="info__showingDetailCinema">
                        {/* <a
                          data-toggle="collapse"
                          href={`#${rap.maCumRap}`}
                          role="button"
                          aria-expanded="false"
                          aria-controls={rap.maCumRap}
                        /> */}
                        <a href="#">[chi tiết]</a>
                      </p>
                    </div>
                    <div className="collapse" id={rap.maCumRap}>
                      <div className="info__time">
                        <p className="info__2D">2D Digital</p>
                        {rap.lichChieuPhim?.map((lichChieu, index) => {
                          return (
                            <NavLink type="button" to={`/checkout/${lichChieu.maLichChieu}`}>
                              <span className="info__timeBegin">
                                {moment(lichChieu.ngayChieuGioChieu).format(
                                  "DD/MM/yyyy - "
                                )}
                                {moment(lichChieu.ngayChieuGioChieu).format(
                                  "hh:mm A"
                                )}
                              </span>
                            </NavLink>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                  <p />
                </div>
              );
            })}
          </div>
        </div>
      );
    });
  };

  return (
    <section className="info">
      <div className="row info__onMobile">
        <div className="col-ms-12">
          <div className="info__text">
            <p className="info__date">
              {moment(phim.ngayKhoiChieu).format("DD-MM-yyyy")}
            </p>
            <p className="info__film">
              <span>{phim.maNhom}</span> {phim.tenPhim}
            </p>
            {renderTime()}
          </div>
        </div>
      </div>
      <div className="info__content">
        <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
          <li className="nav-item">
            <a
              className="nav-link active"
              id="pills-home-tab"
              data-toggle="pill"
              href="#pills-home"
              role="tab"
              aria-controls="pills-home"
              aria-selected="true"
            >
              Thông Tin
            </a>
          </li>
          <li className="nav-item">
            <a
              className="nav-link"
              id="pills-profile-tab"
              data-toggle="pill"
              href="#pills-profile"
              role="tab"
              aria-controls="pills-profile"
              aria-selected="false"
            >
              Đánh Giá
            </a>
          </li>
          <li className="nav-item">
            <a
              className="nav-link"
              id="pills-calendar-tab"
              data-toggle="pill"
              href="#pills-calendar"
              role="tab"
              aria-controls="pills-calendar"
              aria-selected="false"
            >
              Lịch Chiếu
            </a>
          </li>
        </ul>
        <div className="tab-content" id="pills-tabContent">
          <div
            className="tab-pane fade show active"
            id="pills-home"
            role="tabpanel"
            aria-labelledby="pills-home-tab"
          >
            <div className="row info__row1">
              <div className="col-md-6 col-sm-12 info__colLeft">
                <div className="info__rowLeftInfo">
                  <p className="info__contentTitle">Ngày phát hành</p>
                  <p className="info__contentInfo">
                    {moment(phim.ngayKhoiChieu).format("DD-MM-yyyy")}
                  </p>
                </div>
                <div className="info__rowLeftInfo">
                  <p className="info__contentTitle">Đạo diễn</p>
                  <p className="info__contentInfo">Dave Wilson</p>
                </div>
                <div className="info__rowLeftInfo">
                  <p className="info__contentTitle">Diễn viên</p>
                  <p className="info__contentInfo">
                    Toby Kebbell, Eiza González, Vin Diesel
                  </p>
                </div>
                <div className="info__rowLeftInfo">
                  <p className="info__contentTitle">Thể loại</p>
                  <p className="info__contentInfo">Hành động</p>
                </div>
                <div className="info__rowLeftInfo">
                  <p className="info__contentTitle">Định dạng</p>
                  <p className="info__contentInfo">2D/Digital</p>
                </div>
                <div className="info__rowLeftInfo">
                  <p className="info__contentTitle">Quốc Giá SX</p>
                  <p className="info__contentInfo">Mỹ</p>
                </div>
              </div>
              <div className="col-md-6 col-sm-12 info__colRight">
                <p className="info__contentTitle">Nội dung</p>
                <p className="info__contentInfo">{phim.moTa}</p>
              </div>
            </div>
          </div>
          <div
            className="tab-pane fade"
            id="pills-profile"
            role="tabpanel"
            aria-labelledby="pills-profile-tab"
          >
            <div className="info__showReview">
              <div className="row">
                <div className="col-12">
                  <span className="info__imgReviewer">
                    <img src="../img/avatar.png" alt="avatar" />
                  </span>
                  <input
                    type="text"
                    name
                    id
                    className="info__inputReview"
                    placeholder="Bạn nghĩ gì về phim này?"
                  />
                  <span className="info__imgReviewerStar">
                    <img src="../img/listStar.png" alt="listStar" />
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div
            className="tab-pane fade"
            id="pills-calendar"
            role="tabpanel"
            aria-labelledby="pills-calendar-tab"
          >
            <div className="info__calendar">
              <div className="row">
                <div className="col-12">
                  <div className="parentListPCinemas">
                    <div
                      className="nav flex-column nav-pills"
                      id="v-pills-tab"
                      role="tablist"
                      aria-orientation="vertical"
                    >
                      {renderLogoCinema()}
                    </div>
                  </div>
                  <div className="listCinemas">
                    <div className="tab-content" id="v-pills-tabContent">
                      <div
                        className="tab-pane fade show active"
                        id="v-pills-cinema"
                        role="tabpanel"
                        aria-labelledby="v-pills-cinema-tab"
                      >
                        <div className="info__scope">
                          <ul
                            className="nav nav-pills mb-3"
                            id="pills-tab"
                            role="tablist"
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link active"
                                id="pills-monday-tab"
                                data-toggle="pill"
                                href="#pills-monday"
                                role="tab"
                                aria-controls="pills-monday"
                                aria-selected="true"
                              >
                                Thứ 2
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                id="pills-tuesday-tab"
                                data-toggle="pill"
                                href="#pills-tuesday"
                                role="tab"
                                aria-controls="pills-tuesday"
                                aria-selected="false"
                              >
                                Thứ 3
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                id="pills-wednesday-tab"
                                data-toggle="pill"
                                href="#pills-wednesday"
                                role="tab"
                                aria-controls="pills-wednesday"
                                aria-selected="false"
                              >
                                Thứ 4
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                id="pills-thursday-tab"
                                data-toggle="pill"
                                href="#pills-thursday"
                                role="tab"
                                aria-controls="pills-thursday"
                                aria-selected="false"
                              >
                                Thứ 5
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                id="pills-friday-tab"
                                data-toggle="pill"
                                href="#pills-friday"
                                role="tab"
                                aria-controls="pills-friday"
                                aria-selected="false"
                              >
                                Thứ 6
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                id="pills-saturday-tab"
                                data-toggle="pill"
                                href="#pills-saturday"
                                role="tab"
                                aria-controls="pills-saturday"
                                aria-selected="false"
                              >
                                Thứ 7
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link"
                                id="pills-sunday-tab"
                                data-toggle="pill"
                                href="#pills-sunday"
                                role="tab"
                                aria-controls="pills-sunday"
                                aria-selected="false"
                              >
                                Chủ nhật
                              </a>
                            </li>
                          </ul>
                          <div
                            className="tab-content selectScroll"
                            id="pills-tabContent"
                          >
                            {renderDate()}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default DetailInfo;
