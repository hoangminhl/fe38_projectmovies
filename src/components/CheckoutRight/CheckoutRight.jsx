import React, { useState, useEffect } from "react";
import "./CheckoutRight.scss";
import { userLogin } from "../../config/setting";
import { Redirect } from "react-router-dom";
import { qlPhimService } from "../../services/QuanLyPhimService";
import { qlNguoiDung } from "../../services/QuanLyNguoiDungService";
import swal from "sweetalert";

const CheckoutRight = (props) => {
  let [thongTinPhongVe, setThongTinPhongVe] = useState([]);

  useEffect(() => {
    qlPhimService
      .layThongTinPhongVe(props.maLichChieu)
      .then((result) => {
        // console.log(result.data)
        setThongTinPhongVe(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  const datVe = () => {
    let thongTinDatVe = {
      maLichChieu: props.maLichChieu,
      danhSachVe: props.danhSachGheDangDat,
      taiKhoanNguoiDung: JSON.parse(localStorage.getItem("userLogin")).taiKhoan,
    };
    console.log(thongTinDatVe);

    qlNguoiDung
      .datVe(thongTinDatVe)
      .then((result) => {
        console.log(result.data);
        // alert("Đặt vé thành công !");
        swal({
          title: "Bạn có chắc chưa ?",
          // text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            swal("Đặt vé thành công !", {
              icon: "success",
            });
            setTimeout(function () {
              window.location.reload();
            }, 2000);
          } else {
            swal("Chọn lại nào !");
          }
        });

       
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  };

  const renderThongTinGheDangDat = () => {
    return (
      <div>
        {props.danhSachGheDangDat.map((gheDangDat, index) => {
          return (
            <span key={index} className="mr-1">
              Ghế: {gheDangDat.tenGhe} -{" "}
            </span>
          );
        })}
      </div>
    );
  };

  if (!localStorage.getItem(userLogin)) {
    alert("Đăng nhập để đặt vé !");
    return <Redirect to="/login" />;
  }

  return (
    <div className="right">
      <div className="right__content">
        <div className="row right__total">
          <div className="col-12">
            <p className="right__cash">
              {props.danhSachGheDangDat
                .reduce((tongTien, gheDangDat, index) => {
                  return (tongTien += gheDangDat.giaVe);
                }, 0)
                .toLocaleString()}{" "}
              đ
            </p>
          </div>
        </div>
        <div className="row right__filmName">
          <div className="col-12 right__text">
            <p className="right__name">
              <span>{thongTinPhongVe.thongTinPhim?.tenRap}</span>{" "}
              {thongTinPhongVe.thongTinPhim?.tenPhim}
            </p>
            <div className="right__cinemaName">
              <span className="cinemaName">
                {thongTinPhongVe.thongTinPhim?.tenCumRap}
              </span>
            </div>
            <div className="right__hour">
              Ngày {thongTinPhongVe.thongTinPhim?.ngayChieu} -{" "}
              {thongTinPhongVe.thongTinPhim?.gioChieu}
            </div>
            <div className="right__address">
              {thongTinPhongVe.thongTinPhim?.diaChi}
            </div>
          </div>
        </div>
        <div className="row right__chair">
          <div className="col-12">
            <div className="right__left">
              Đặt ghế:{" "}
              <span className="gheDat">{renderThongTinGheDangDat()}</span>
            </div>
            <div className="right__right">
              {props.danhSachGheDangDat
                .reduce((tongTien, gheDangDat, index) => {
                  return (tongTien += gheDangDat.giaVe);
                }, 0)
                .toLocaleString()}{" "}
              đồng
            </div>
          </div>
        </div>
        <div className="row right__infoUser">
          <div className="col-12">
            <input type="text" className="content" />
            <label htmlFor="emailCheckout">E-Mail</label>
          </div>
        </div>
        <div className="row right__infoUser">
          <div className="col-12">
            <input type="text" className="content" />
            <label htmlFor="phoneCheckout">Phone</label>
          </div>
        </div>
        <div className="row right__voucher">
          <div className="col-12">
            <div className="right__left">
              <input type="text" placeholder="Nhập tại đây..." />
              <label htmlFor="voucherPromotion">Mã giảm giá</label>
            </div>
            <div className="right__right">
              <div className="right__apply">Áp dụng</div>
            </div>
          </div>
        </div>
        <div className="row right__methodPay">
          <div className="col-12">
            <p className="right__titleMethodPay">Hình thức thanh toán</p>
            <p className="right__warning">
              Vui lòng chọn ghế để hiển thị phương thức thanh toán phù hợp.
            </p>
          </div>
        </div>
        <div className="row right__notice">
          <div className="right__errorBook">
            <img src="../img/exclamation.png" alt="iconExclamation" />
            <span className="right__title">
              Vé đã mua không thể đổi hoặc hoàn tiền <br />
            </span>
            <span className="right__title">
              Mã vé sẽ được gửi qua tin nhắn{" "}
              <span className="right__item">ZMS</span> (tin nhắn Zalo) và <br />{" "}
              <span className="right__item">Email</span> đã nhập
            </span>
          </div>
        </div>
      </div>
      <div className="right__button">
        <button
          className="button"
          onClick={() => {
            datVe();
          }}
        >
          Đặt Vé
        </button>
      </div>
    </div>
  );
};

export default CheckoutRight;
