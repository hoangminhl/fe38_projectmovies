import React, { Fragment } from "react";
import "./CheckoutHeader.scss";
import { useSelector, useDispatch } from "react-redux";
import { dangXuatAction } from "../../redux/actions/quanLyNguoiDungAction";
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import { makeStyles } from "@material-ui/core/styles";
import { NavLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  paper: {
    marginRight: theme.spacing(2),
  },
}));

const CheckoutHeader = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    }
  }

  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  const taiKhoan = useSelector(
    (state) => state.QuanLyNguoiDungReducer.taiKhoan
  );
  // console.log("hookselect", taiKhoan);

  const dispatch = useDispatch();

  const logOut = () => {
    dispatch(dangXuatAction());
  };

  const renderAdmin = () => {
    if(JSON.parse(localStorage.getItem("userLogin")).maLoaiNguoiDung === "QuanTri"){
      return (
        <MenuItem onClick={handleClose}>
          <NavLink to="/moviemanagement" style={{textDecoration: "none", color: '#333'}}>
          <i class="fas fa-user"></i> Admin
          </NavLink>
        </MenuItem>
      )
    }
    else{
      return null
    }
  }

  const renderLogin = () => {
    if (taiKhoan !== "") {
      return (
        <Fragment>
          <Button
            className="header__signIn"
            ref={anchorRef}
            aria-controls={open ? "menu-list-grow" : undefined}
            aria-haspopup="true"
            onClick={handleToggle}
          >
            {taiKhoan}
          </Button>
          <Popper
            open={open}
            anchorEl={anchorRef.current}
            role={undefined}
            transition
            disablePortal
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom",
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={handleClose}>
                    <MenuList
                      autoFocusItem={open}
                      id="menu-list-grow"
                      onKeyDown={handleListKeyDown}
                    >
                      {renderAdmin()}
                      <MenuItem onClick={handleClose}>
                        <NavLink to="/userinformation" style={{textDecoration: "none", color: "#333"}}>
                          <i className="fa fa-user mr-1"></i>Profile
                        </NavLink>
                      </MenuItem>
                      <MenuItem onClick={logOut}>
                        <i className="fa fa-sign-out-alt mr-1"></i>Logout
                      </MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
        </Fragment>
      );
    }
    return (
      <NavLink to="/login" className="header__signIn">
        Đăng Nhập
      </NavLink>
    );
  };

  return (
    <div className="header">
      <div className="header__content">
        <div className="row header__hideOnMobile">
          <div className="header__colLeft">
            <ul className="form">
              <li className="logo">
                <NavLink to="/" className="header__webLogo nav-link">
                  <img
                    src="https://tix.vn/app/assets/img/icons/web-logo.png"
                    alt="web-logo"
                  />
                </NavLink>
              </li>
              <li className="active items">
                <span>01 </span> CHỌN GHẾ &amp; THANH TOÁN
              </li>
            </ul>
          </div>
          <div className="header__colRight">
            <div className="header__user">
              <img src="../img/avatar.png" alt="avatar" />
              <span>{renderLogin()}</span>
            </div>
          </div>
        </div>
        <button
          className="header__mobile navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#sideMenu"
          aria-controls="navbarSupportedContent"
          aria-expanded="true"
          aria-label="Toggle navigation"
        >
          <img src="../img/align-left.png" alt />
        </button>
        <div
          id="sideMenu"
          className="header__sideMenu collapse navbar-collapse"
        >
          <ul className="navbar-nav">
            <li className="active">
              <span>01 </span> CHỌN GHẾ &amp; THANH TOÁN
            </li>
          </ul>
        </div>
        <div className="header__responsivePay">
          <h6>02. THANH TOÁN</h6>
        </div>
      </div>
    </div>
  );
};

export default CheckoutHeader;
