import React, { Fragment } from "react";
import { Dialog } from "@material-ui/core";
import "./TrailerMovie.scss"

const TrailerMovie = (props) => {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Fragment>
      <img src="../img/play-video.png" alt="play-video" onClick={handleClickOpen}/>

      <Dialog
        fullWidth={true}
        maxWidth="md"
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <iframe
          height="800px"
          allowFullScreen
          frameBorder="0"
          src={props.trailer}
        ></iframe>
      </Dialog>
    </Fragment>
  );
};

export default TrailerMovie;
