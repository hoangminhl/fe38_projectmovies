import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import "../Header/Header.scss";
import { useSelector, useDispatch } from "react-redux";
import { dangXuatAction } from "../../redux/actions/quanLyNguoiDungAction";
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  paper: {
    marginRight: theme.spacing(2),
  },
}));

const Header = (props) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    }
  }

  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  const taiKhoan = useSelector(
    (state) => state.QuanLyNguoiDungReducer.taiKhoan
  );
  // console.log("hookselect", taiKhoan);

  const dispatch = useDispatch();

  const logOut = () => {
    dispatch(dangXuatAction());
  };

  const renderAdmin = () => {
    if (
      JSON.parse(localStorage.getItem("userLogin")).maLoaiNguoiDung ===
      "QuanTri"
    ) {
      return (
        <MenuItem onClick={handleClose}>
          <NavLink
            to="/moviemanagement"
            style={{ textDecoration: "none", color: "#333" }}
          >
            <i class="fas fa-user"></i> Admin
          </NavLink>
        </MenuItem>
      );
    } else {
      return null;
    }
  };

  const renderLoginReponsive = () => {
    if (taiKhoan !== "") {
      return <span>{taiKhoan}</span>;
    } else {
      return <NavLink to="/login">Đăng nhập</NavLink>;
    }
  };

  const renderLogoutResponsive = () => {
    if (taiKhoan !== "") {
      return (
        <span className="header__text" onClick={logOut}>
          <i className="fa fa-sign-out-alt mr-1"></i>Logout
        </span>
      );
    } else {
      return null;
    }
  };

  const renderAdminResponsive = () => {
    if (taiKhoan !== "") {
      return <Fragment>{renderAdmin()}</Fragment>;
    }
  };

  const renderLogin = () => {
    if (taiKhoan !== "") {
      return (
        <Fragment>
          <Button
            className="header__signIn"
            ref={anchorRef}
            aria-controls={open ? "menu-list-grow" : undefined}
            aria-haspopup="true"
            onClick={handleToggle}
          >
            {taiKhoan}
          </Button>
          <Popper
            open={open}
            anchorEl={anchorRef.current}
            role={undefined}
            transition
            disablePortal
            style={{ zIndex: "999" }}
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom",
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={handleClose}>
                    <MenuList
                      autoFocusItem={open}
                      id="menu-list-grow"
                      onKeyDown={handleListKeyDown}
                    >
                      {renderAdmin()}
                      <MenuItem onClick={handleClose}>
                        <NavLink
                          to="/userinformation"
                          style={{ textDecoration: "none", color: "#333" }}
                        >
                          <i className="fa fa-user mr-1"></i>Profile
                        </NavLink>
                      </MenuItem>
                      <MenuItem onClick={logOut}>
                        <i className="fa fa-sign-out-alt mr-1"></i>Logout
                      </MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
        </Fragment>
      );
    }
    return (
      <NavLink to="/login" className="header__signIn">
        Đăng Nhập
      </NavLink>
    );
  };

  return (
    <header>
      <div className="header__content">
        <div className="header__left">
          <NavLink to="/" className="header__webLogo nav-link">
            <img
              src="https://tix.vn/app/assets/img/icons/web-logo.png"
              alt="web-logo"
            />
          </NavLink>
        </div>
        <div className="header__center">
          <a
            href="#filmBlock"
            className="header__text jumper"
            data-scroll="homeMovies"
          >
            Lịch Chiếu
          </a>
          <a href="#cinemaBlock" className="header__text jumper">
            Cụm rạp
          </a>
          <a href="#newsBlock" className="header__text jumper">
            Tin Tức
          </a>
          <a href="#homeApp" className="header__text jumper">
            Ứng dụng
          </a>
        </div>
        <div className="header__right">
          <div className="header__account header__logIn">
            <img
              src="https://i.ibb.co/PCjW83Y/avt.png"
              alt="avatar"
              className="header__avatar"
            />
            {renderLogin()}
          </div>
          <div className="header__account dropdown header__location">
            <div
              className="dropdown-toggle"
              data-toggle="dropdown"
              aria-expanded="false"
            >
              Hồ Chí Minh
            </div>
            <ul className="dropdown-menu selectScroll">
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Hồ Chí Minh
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Hà Nội
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Đà Nẵng
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Hải Phòng
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Biên Hòa
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Nha Trang
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Bình Dương
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Phan Thiết
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Hạ Long
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Cần Thơ
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Vũng Tàu
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Quy Nhơn
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Huế
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Long Xuyên
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Thái Nguyên
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Buôn Ma Thuột
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Bắc Giang
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Bến Tre
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Việt Trì
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Ninh Bình
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Thái Bình
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Vinh
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Bảo Lộc
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Đà Lạt
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Trà Vinh
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Yên Bái
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Kiên Giang
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Vĩnh Long
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Cà Mau
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Hậu Giang
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Tây Ninh
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Tuyên Quang
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Thanh Hóa
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Nam Định
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Hải Dương
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Gia Lai
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Hà Tĩnh
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Phú Yên
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Bạc Liêu
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Long An
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Đồng Hới
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Hà Nam
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Bắc Ninh
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Quảng Trị
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Kon Tum
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Sóc Trăng
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Sơn La
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Lạng Sơn
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Quảng Ngãi
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Mỹ Tho
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Đồng Tháp
                </a>
              </li>
              <li className="ng-scope">
                <a href="#" className="ng-bingding">
                  Hưng Yên
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="header__leftMobile">
          <NavLink to="/" className="header__webLogo">
            <img
              src="https://tix.vn/app/assets/img/icons/web-logo.png"
              alt="web-logo"
            />
          </NavLink>
        </div>
        <button
          className="header__rightMobile navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#sideMenu"
          aria-controls="navbarSupportedContent"
          aria-expanded="true"
          aria-label="Toggle navigation"
        >
          <img
            src="https://tix.vn/app/assets/img/icons/menu-options.png"
            alt="menu-options"
          />
        </button>
        <div id="sideMenu" className="header__sideMenu">
          <div className="wrapMenuMobile selectScroll">
            <div className="header__wrapFirst">
              <p className="header__titleMenu">
                <img
                  src="https://tix.vn/app/assets/img/avatar.png"
                  alt="avatar"
                />
                {renderLoginReponsive()}
              </p>
              <img
                src="https://tix.vn/app/assets/img/icons/next-session.png"
                alt="next-session"
                className="close"
                data-toggle="collapse"
                data-target="#sideMenu"
              />
            </div>
            {/* <a
              href="#filmBlock"
              className="header__text jumper"
              data-scroll="homeMovies"
            >
              Lịch Chiếu
            </a>
            <a href="#cinemaBlock" className="header__text jumper">
              Cụm rạp
            </a>
            <a href="#newsBlock" className="header__text jumper">
              Tin Tức
            </a>
            <a href="#homeApp" className="header__text jumper">
              Ứng dụng
            </a>
            <a href="#" className="header__text">
              Hồ Chí Minh
            </a> */}
            {renderAdminResponsive()}
            <NavLink
              to="/userinformation"
              style={{ textDecoration: "none", color: "#333" }}
              className="header__text"
            >
              <i className="fa fa-user mr-1"></i>Profile
            </NavLink>
            {renderLogoutResponsive()}
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
