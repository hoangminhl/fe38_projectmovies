import React, { useEffect, useState, Fragment } from "react";
import "./Calendar.scss";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { qlPhimService } from "../../services/QuanLyPhimService";
import TrailerMovie from "../DialogMovie/TrailerMovie";
import { NavLink } from "react-router-dom";

const Calendar = () => {
  let [danhSachPhim, setDanhSachPhim] = useState([]);
  let [danhSachPhimSC, setDanhSachPhimSC] = useState([]);
  var setting = {
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    // slidesPerRow: 3,
    rows: 2,
  };

  useEffect(() => {
    qlPhimService
      .layDanhSachPhim()
      .then((result) => {
        setDanhSachPhim(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  useEffect(() => {
    qlPhimService
      .layDanhSachPhimSapChieu()
      .then((result) => {
        setDanhSachPhimSC(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  const renderPhim = () => {
    return danhSachPhim.map((phim, index) => {
      return (
        <Fragment key={index}>
          <div className="calendar__col3">
            <div className="calendar__film">
              <div className="calendar__picture">
                <div
                  className="calendar__filmThumbnail"
                  style={{
                    backgroundImage: `url("${phim.hinhAnh}")`,
                  }}
                >
                  <NavLink className="calendar__overlay" to="./home">
                    <button type="button" className="playTrailer">
                      <TrailerMovie trailer={phim.trailer} />
                    </button>
                  </NavLink>
                  <span className="calendar__ageType">{phim.maNhom}</span>
                  <div className="calendar__avgPoint">
                    <p className="calendar__point">{phim.danhGia}</p>
                    <p className="calendar__star">
                      <img src="../img/star1.png" alt="star1" />
                      <img src="../img/star1.png" alt="star1" />
                      <img src="../img/star1.png" alt="star1" />
                      <img src="../img/star1.2.png" alt="star1.2" />
                    </p>
                  </div>
                </div>
              </div>

              <div className="calendar__text">
                <div className="calender__button">
                  <NavLink
                    to={`/moviedetail/${phim.maPhim}`}
                    className="btn calender__button-booking"
                  >
                    Đặt vé
                  </NavLink>
                </div>
                <p className="calendar__nameFilm">{phim.tenPhim} (C18)</p>
                <p className="calendar__time">110 phút</p>
              </div>
            </div>
          </div>
        </Fragment>
      );
    });
  };

  const renderPhimSapChieu = () => {
    return danhSachPhimSC.map((phimSC, index) => {
      return (
        <Fragment key={index}>
          <div className="calendar__col3">
            <div className="calendar__film">
              <NavLink className="calendar__picture" to="./home">
                <div
                  className="calendar__filmThumbnail"
                  style={{
                    backgroundImage: `url("${phimSC.hinhAnh}")`,
                  }}
                >
                  <div className="calendar__overlay">
                    <button type="button" className="playTrailer">
                      <TrailerMovie trailer={phimSC.trailer} />
                    </button>
                  </div>
                  <span className="calendar__ageType-1">{phimSC.maNhom}</span>
                  <div className="calendar__avgPoint">
                    <p className="calendar__point">{phimSC.danhGia}</p>
                    <p className="calendar__star">
                      <img src="../img/star1.png" alt="star1" />
                      <img src="../img/star1.png" alt="star1" />
                      <img src="../img/star1.png" alt="star1" />
                      <img src="../img/star1.2.png" alt="star1.2" />
                    </p>
                  </div>
                </div>
              </NavLink>

              <div className="calendar__text">
                <div className="calender__button">
                  <NavLink
                    to={`/moviedetail/${phimSC.maPhim}`}
                    className="btn calender__button-booking"
                  >
                    Đặt vé
                  </NavLink>
                </div>
                <p className="calendar__nameFilm">{phimSC.tenPhim} (C18)</p>
                <p className="calendar__time">110 phút</p>
              </div>
            </div>
          </div>
        </Fragment>
      );
    });
  };

  return (
    <section className="calendar" id="filmBlock">
      <div className="calendar__bg" id="homeMovies">
        <div className="calendar__content">
          <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li className="nav-item">
              <a
                className="nav-link active"
                id="pills-home-tab"
                data-toggle="pill"
                href="#pills-home"
                role="tab"
                aria-controls="pills-home"
                aria-selected="true"
              >
                Đang Chiếu
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                id="pills-profile-tab"
                data-toggle="pill"
                href="#pills-profile"
                role="tab"
                aria-controls="pills-profile"
                aria-selected="false"
              >
                Sắp Chiếu
              </a>
            </li>
          </ul>
          <div className="tab-content calendar__content" id="pills-tabContent">
            <div
              className="tab-pane fade show active owl-carousel calender__pane"
              id="pills-home"
              role="tabpanel"
              aria-labelledby="pills-home-tab"
            >
              <Slider {...setting}>{renderPhim()}</Slider>
            </div>
            <div
              className="tab-pane fade owl-carousel"
              id="pills-profile"
              role="tabpanel"
              aria-labelledby="pills-profile-tab"
            >
              <Slider {...setting}>{renderPhimSapChieu()}</Slider>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Calendar;
