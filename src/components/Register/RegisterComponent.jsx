import React, { useState } from "react";
import "./RegisterComponent.scss";
import { NavLink } from "react-router-dom";
import swal from "sweetalert";
import { qlNguoiDung } from "../../services/QuanLyNguoiDungService";
import { groupID } from "../../config/setting";

const RegisterComponent = (props) => {
  let [state, setState] = useState({
    values: {
      hoTen: "",
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDienThoai: "",
      maLoaiNguoiDung: "KhachHang",
      maNhom: groupID
    },

    errors: {
      hoTen: "",
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDienThoai: "",
    },
  });

  const handleChangeInput = (event) => {
    let { value, name } = event.target;
    let newValues = {
      ...state.values,
      [name]: value,
    };

    let newErrors = {
      ...state.errors,
      [name]: value === "" ? "Không được bỏ trống!" : "",
    };

    if (name === "email") {
      let regexEmail = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
      if (value.match(regexEmail)) {
        newErrors.email = "";
      } else {
        newErrors.email = "Email không hợp lệ!";
      }
    }

    setState({ values: newValues, errors: newErrors });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let valid = true;
    let { values, errors } = state;
    console.log(values)

    for (let key in values) {
        console.log(values, key)
      if (values[key] === "") {
        valid = false;
      }
    }
    for (let key in errors) {
      if (errors[key] !== "") {
        valid = false;
      }
    }
    if (!valid) {
      swal({
        title: "Thông tin không hợp lệ!",
        icon: "error",
        button: "OK",
      });
      return;
    }

    qlNguoiDung
      .dangKy(values)
      .then((result) => {
          console.log(result.data)
        swal({
          title: "Đăng ký thành công!",
          icon: "success",
          button: "OK",
        });
        props.thongTin.history.push("/login");
      })
      .catch((errors) => {
        swal({
          title: errors.response.data,
          text: "Nhập lại thông tin",
          icon: "warning",
          button: "OK",
        });
      });
  };

  return (
    <div className="register">
      <div
        className="register__content"
        style={{ backgroundImage: "url(./img/backapp.jpg)" }}
      >
        <div className="register__cover">
          <form className="register__signIn animLogin" onSubmit={handleSubmit}>
            <img src="../img/group@2x.png" alt="iconGroup@2x" />
            <div className="register__text">
              <p>
                Đăng nhập để được nhiều ưu đãi, mua vé <br />
                và bảo mật thông tin!
              </p>
            </div>
            <div className="row register__social">
              <div className="col-md-12 form-group">
                <input
                  name="taiKhoan"
                  className="form-control"
                  placeholder="Tài khoản"
                  onChange={handleChangeInput}
                />
                <span className="text-danger">{state.errors.taiKhoan}</span>
              </div>
              <div className="col-md-12 form-group">
                <input
                  type="password"
                  name="matKhau"
                  className="form-control"
                  placeholder="Mật khẩu"
                  onChange={handleChangeInput}
                />
                <span className="text-danger">{state.errors.matKhau}</span>
              </div>
              <div className="col-md-12 form-group">
                <input
                  name="hoTen"
                  className="form-control"
                  placeholder="Họ tên"
                  onChange={handleChangeInput}
                />
                <span className="text-danger">{state.errors.hoTen}</span>
              </div>
              <div className="col-md-12 form-group">
                <input
                  name="email"
                  className="form-control"
                  placeholder="Email"
                  onChange={handleChangeInput}
                />
                <span className="text-danger">{state.errors.email}</span>
              </div>
              <div className="col-md-12 form-group">
                <input
                  name="soDienThoai"
                  className="form-control"
                  placeholder="Số điện thoại"
                  onChange={handleChangeInput}
                />
                <span className="text-danger">{state.errors.soDienThoai}</span>
              </div>
              <div className="col-md-12 form-group">
                <button className="btn btn-dark btn-block">Đăng ký</button>
              </div>
              <div className="register__dangNhap">
                <NavLink to="/login" className="login">
                  Bạn đã có tài khoản?
                </NavLink>
              </div>
            </div>
            <NavLink to="/home" className="register__signClose" />
          </form>
        </div>
      </div>
    </div>
  );
};
export default RegisterComponent;
