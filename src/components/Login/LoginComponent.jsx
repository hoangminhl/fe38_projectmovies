import React, { useState } from "react";
import "./LoginComponent.scss";
import { userLogin, token } from "../../config/setting";
import { useDispatch } from "react-redux";
import { qlNguoiDung } from "../../services/QuanLyNguoiDungService";
import { dangNhapAction } from "../../redux/actions/quanLyNguoiDungAction";
import { NavLink } from "react-router-dom";

const LoginComponent = (props) => {
  console.log(props)
  const dispatch = useDispatch();
  let [state, setState] = useState({
    values: {
      taiKhoan: "",
      matKhau: "",
    },
    errors: {
      taiKhoan: "",
      matKhau: "",
    },
  });

  let [result, setResult] = useState("Chưa đăng nhập");

  const handleChangeInput = (event) => {
    let { value, name } = event.target;
    let newValue = {
      ...state.values,
      [name]: value,
    };

    let newErrors = {
      ...state.errors,
      [name]: value === "" ? "Không được bỏ trống!" : "",
    };

    setState({ values: newValue, errors: newErrors });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    qlNguoiDung
      .dangNhap(state.values)
      .then((result) => {
        console.log(result.data);
        //Lưu thông tin user và localStorage
        localStorage.setItem(userLogin, JSON.stringify(result.data));
        //Lưu thông tin token vào localStorage
        localStorage.setItem(token, result.data.accessToken);
        dispatch(dangNhapAction(result.data.taiKhoan));

        props.thongTin.history.push("/home");
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });

    setResult("Đăng nhập không thành công !");
  };

  return (
    <div className="login">
      <div
        className="login__content"
        style={{ backgroundImage: "url(./img/backapp.jpg)" }}
      >
        <div className="login__cover">
          <form className="login__signIn animLogin" onSubmit={handleSubmit}>
            <img src="../img/group@2x.png" alt="iconGroup@2x" />
            <div className="login__text">
              <h2 className="text-center text-danger">{result}</h2>
              <p>
                Đăng nhập để được nhiều ưu đãi, mua vé <br />
                và bảo mật thông tin!
              </p>
            </div>
            <div className="row login__social">
              <div className="col-md-12 form-group">
                <input
                  name="taiKhoan"
                  className="form-control"
                  placeholder="Tài khoản"
                  onChange={handleChangeInput}
                />
                <span className="text-danger">{state.errors.taiKhoan}</span>
              </div>
              <div className="col-md-12 form-group">
                <input
                  type="password"
                  name="matKhau"
                  className="form-control"
                  placeholder="Mật khẩu"
                  onChange={handleChangeInput}
                />
                <span className="text-danger">{state.errors.matKhau}</span>
              </div>
              <div className="col-md-12 form-group">
                <button className="btn btn-dark btn-block">Đăng nhập</button>
              </div>
              <div className="login__dangKy">
                <NavLink to="/register" className="register">Bạn chưa có tài khoản?</NavLink>
              </div>
            </div>
            <NavLink to="/home" className="login__signClose" />
          </form>
        </div>
      </div>
    </div>
  );
};

export default LoginComponent;
