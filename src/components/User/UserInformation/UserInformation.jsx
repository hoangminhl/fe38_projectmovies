import React from 'react';
import "./UserInformation.scss";
import { userLogin } from '../../../config/setting';
import { Redirect } from 'react-router-dom';

const UserInformation = () => {
    let thongTin = JSON.parse(localStorage.getItem("userLogin"))

    if(!localStorage.getItem(userLogin)){
        alert("Đăng nhập để xem thông tin!")
        return <Redirect to="/login"/>
    }

    return (
        <div className="UserInfo__content">
            <h2 className="UserInfo__title text-light">Thông tin cá nhân</h2>
            <form>
                <table className="table bg-light">
                    <tbody className="rounded">
                        <tr>
                            <th>Tài khoản:</th>
                            <td>{thongTin.taiKhoan}</td>
                        </tr>
                        <tr>
                            <th>Họ tên:</th>
                            <td>{thongTin.hoTen}</td>
                        </tr>
                        <tr>
                            <th>Email:</th>
                            <td>{thongTin.email}</td>
                        </tr>
                        <tr>
                            <th>Số điện thoại:</th>
                            <td>{thongTin.soDT}</td>
                        </tr>
                        <tr>
                            <th><i class="fas fa-user"></i></th>
                            <td>{thongTin.maLoaiNguoiDung}</td>
                        </tr>
                    </tbody>
                   
                </table>
            </form>
        </div>
    )
}

export default UserInformation