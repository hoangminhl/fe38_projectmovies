import React, { Fragment, useState, useEffect } from 'react';
import UserInformation from './UserInformation/UserInformation';
import TicketBooking from './ticketBooking/TicketBooking';
import { useSelector } from 'react-redux';
import { qlNguoiDung } from '../../services/QuanLyNguoiDungService';

const User = () => {
    let [thongTin, setThongTin] = useState ([]);
    console.log(thongTin)

    useEffect(() => {
        qlNguoiDung.layThongTinTaiKhoan(JSON.parse(localStorage.getItem("userLogin"))).then((result) => {
            setThongTin(result.data)
        })
    }, [])

    return (
        <Fragment>
            <div className="container-fluid bg-dark" style={{padding: "120px 20px"}}>
                <div className='row'>
                    <div className="col-4">
                        <UserInformation/>
                    </div>
                    <div className="col-8">
                        <TicketBooking thongTin={thongTin}/>
                    </div>
                </div>
            </div>    
        </Fragment>
    )
}

export default User