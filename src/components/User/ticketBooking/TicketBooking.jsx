import React from "react";
import "./TicketBooking.scss";

var moment = require("moment");

const TicketBooking = (props) => {
  let thongTinDatVe = props.thongTin.thongTinDatVe;
  console.log(thongTinDatVe);

  const renderThongTinDatVe = () => {
    return thongTinDatVe?.map((veDat, index) => {
      return (
        <tr key={index}>
          <th scope="row">{veDat.maVe}</th>
          <td style={{textAlign:"center"}}>{veDat.tenPhim}</td>
          <td>
            {moment(veDat.ngayDat).format("DD-MM-yyyy")} <br />
            {moment(veDat.ngayDat).format("hh:mm A")}
          </td>
          <td>
            {veDat.danhSachGhe?.map((ghe, index) => {
              return <p key={index}>Ghế: {ghe.tenGhe} </p>;
            })}
          </td>
        </tr>
      );
    });
  };

  return (
    <div className="TicketBooking__content">
      <h2 className="TicketBooking___title text-light">Lịch sử đặt vé</h2>
      <div className="TicketBooking__form selectScroll">
        <table className="table bg-light">
          <thead>
            <tr className="bg-dark text-light">
              <th scope="col">Mã vé</th>
              <th scope="col" style={{textAlign:"center"}}>Tên phim</th>
              <th scope="col">Thời gian đặt</th>
              <th scope="col">Số ghế</th>
            </tr>
          </thead>
          <tbody>{renderThongTinDatVe()}</tbody>
        </table>
      </div>
    </div>
  );
};

export default TicketBooking;
