import React from 'react';
import "./DetailBackGround.scss";
import DetailName from '../DetailName/DetailName';
import DetailInfo from '../DetailInfo/DetailInfo';

const DetailBackGround = (props) => {
    let maPhim = props.maPhim
    return (
        <section className="background-detail">
            <DetailName maPhim={maPhim}/>
            <DetailInfo maPhim={maPhim}/>
        </section>
    )
}

export default DetailBackGround